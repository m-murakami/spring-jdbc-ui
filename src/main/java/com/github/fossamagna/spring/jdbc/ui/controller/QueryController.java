package com.github.fossamagna.spring.jdbc.ui.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.github.fossamagna.spring.jdbc.ui.model.QueryRequest;
import com.github.fossamagna.spring.jdbc.ui.model.QueryResult;
import com.github.fossamagna.spring.jdbc.ui.service.QueryService;

/**
 * REST controller for SQL execution request message.
 * 
 * @author fossamagna
 *
 */
@RestController
@RequestMapping("${jdbc.ui.api.query.path:/api/query}")
public class QueryController {
  
  @Autowired
  private QueryService service;

  @RequestMapping(method = RequestMethod.POST)
  public ResponseEntity<QueryResult> execute(@RequestBody QueryRequest request) {
    QueryResult body = service.execute(request);
    return new ResponseEntity<>(body, HttpStatus.OK);
  }
  
  @ExceptionHandler(Throwable.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ResponseBody
  public Map<String, Object> handleError(Throwable e) {
      Map<String, Object> errorMap = new HashMap<String, Object>();
      errorMap.put("message", e.getMessage());
      return errorMap;
  }
}
