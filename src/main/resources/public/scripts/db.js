'use strict';

class DB {
  constructor() {
    const self = this;
    const indexedDB = window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB || window.msIndexedDB;
    if (indexedDB) {
      const openRequest = indexedDB.open('sql-editor');
      openRequest.onerror = (e) => {
        console.log("Database error: " + e.target.errorCode);
      };
      openRequest.onsuccess = (event) => {
        self.db = event.target.result;
        console.log("Database open success: " + self.db);
      };
      openRequest.onupgradeneeded = (event) => {
        // データベースのバージョンに変更があった場合(初めての場合もここを通ります。)
        const db = event.target.result;
        const store = db.createObjectStore("namedQuery", { keyPath: "name"});
        console.log("Database createObjectStore: " + store);
        //
        // // インデックスを作成します。
        // store.createIndex("myvalueIndex", "myvalue");
      };


    } else {
      throw 'unsupported platform IndexedDB.';
    }
  }

  save(type, value, cb) {
    if (!this.db) {
      console.log(this);
      return null;
    }
    var transaction = this.db.transaction(type, "readwrite");
    var store = transaction.objectStore(type);

    var request = store.add(value);
    request.onsuccess = function(e) {
      console.log(e);
      cb(null, e);
    };

     request.onerror = function(e) {
        console.log(e.value);
        cb(e);
     };
  }

  find(type, key, cb) {
    var store = this.db.transaction(type).objectStore(type);
    store.get(key).onsuccess = function(event) {
      cb(null, event.target.result);
    };
  }

  findAll(type, cb) {
    var store = this.db.transaction(type).objectStore(type);
    store.openCursor().onsuccess = function (event) {
        var cursor = event.target.result;
        if (cursor) {
            console.log("id_str:" + cursor.key + " Text: " + JSON.stringify(cursor.value));
            cursor.continue();
        }
    };
  }
}

exports.DB = DB;
