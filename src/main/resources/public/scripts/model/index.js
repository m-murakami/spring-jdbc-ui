'use strict';

import prop from 'mithril-stream';
import Parameter from './parameter';
import Query from './query';
import Result from './result';

export default class Model {
  constructor() {
    this.queries = prop([]),
    this.query = prop(new Query());
    this.result = prop(new Result());
    this.error = prop();
    this.typelist = prop([
      'int',
      'double',
      'java.math.BigDecimal',
      'java.util.Date'
    ]);
  }

  run() {
    this.result(new Result());
    this.error(null);
    return this.query().run()
    .then(r => this.result(r))
    .catch(e => this.error(e));
  }

  addParameter() {
    this.query().parameters().push(new Parameter());
  }

  removeParameter(p) {
    const i = this.query().parameters().indexOf(p);
    this.query().parameters().splice(i, 1);
  }
}
