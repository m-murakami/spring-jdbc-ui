'use strict';

import m from 'mithril';
import CodeMirror from 'codemirror';

const sqlMode = require('codemirror/mode/sql/sql');

export default class SQLEditor {

  onremove(vnode) {
    if (this._onchangehandler) {
      editor.off('change', this._onchangehandler);
    }
  }

  oncreate(vnode) {
    const element = vnode.dom;
    const editor = CodeMirror.fromTextArea(element, {
      lineNumbers: true,
      mode : 'text/x-sql'
    });
    editor.getDoc().setValue(vnode.attrs.value);
    if (typeof vnode.attrs.onchange === 'function') {
      this._onchangehandler = this.createOnchangeHandler(vnode.attrs.onchange);
      editor.on('change', this._onchangehandler);
    }
    vnode.state.editor = editor;
  }

  createOnchangeHandler(callback) {
    return cm => {
      // emulate event object.
      const e = {
        currentTarget: {
          value: cm.getDoc().getValue()
        }
      };
      callback(e);
    };
  }

  view(vnode) {
    return <textarea/>;
  }
}