package com.github.fossamagna.spring.jdbc.ui.model;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Object to represent a table column definition.
 * 
 * @author fossamagna
 */
public class Column {
  private String label;
  private String typeName;
  public String getLabel() {
    return label;
  }
  public void setLabel(String label) {
    this.label = label;
  }
  public String getTypeName() {
    return typeName;
  }
  public void setTypeName(String type) {
    this.typeName = type;
  }
  
  public static List<Column> getColumns(ResultSetMetaData metaData) throws SQLException {
    List<Column> columns = new ArrayList<>();
    for (int i = 1; i <= metaData.getColumnCount(); i++) {
      Column column = new Column();
      column.setLabel(metaData.getColumnLabel(i));
      column.setTypeName(metaData.getColumnTypeName(i));
      columns.add(column);
    }
    return columns;
  }
}