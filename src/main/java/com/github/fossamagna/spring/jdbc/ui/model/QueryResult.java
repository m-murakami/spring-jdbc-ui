package com.github.fossamagna.spring.jdbc.ui.model;

import java.util.List;
import java.util.Map;

/**
 * Represents an result of SQL execution.
 * 
 * @author fossamagna
 */
public class QueryResult {
  private int updateCount;
  private List<Column> columns;
  public List<Column> getColumns() {
    return columns;
  }
  public void setColumns(List<Column> columns) {
    this.columns = columns;
  }
  private List<Map<String, Object>> resultSet;
  public int getUpdateCount() {
    return updateCount;
  }
  public void setUpdateCount(int updateCount) {
    this.updateCount = updateCount;
  }
  public List<Map<String, Object>> getResultSet() {
    return resultSet;
  }
  public void setResultSet(List<Map<String, Object>> resultSet) {
    this.resultSet = resultSet;
  }
}