'use strict';

import m from 'mithril';
import prop from 'mithril-stream';
import Result from './result';

export default class Query {
  constructor() {
    this.sql = prop('');
    this.parameters = prop([]);
  }

  run() {
    return m.request({method: 'POST', url: 'api/query/', data: this, type: Result});
  }
}