package com.github.fossamagna.spring.jdbc.ui.model;

import org.springframework.core.convert.ConversionService;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.util.ClassUtils;

/**
 * Object to represent a SQL parameter definition.
 * 
 * @see SqlParameterSource
 * @author fossamagna
 */
public class Parameter {
  private String name;
  private String value;
  private String type;
  
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Object getConvertedValue(ConversionService conversionService) {
    if (this.type == null || this.type.isEmpty()) {
      return this.value;
    }
    Class<?> cls;
    try {
      cls = ClassUtils.forName(this.type, null);
      return conversionService.convert(this.value, cls);
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    return value;
  }
  
}