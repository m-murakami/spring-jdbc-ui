'use strict';

import m from 'mithril';
import Model from './model';
import SQLEditor from './component/sqleditor';

class App {
  oninit(vnode) {
    this.vm = new Model();
    this.vm.query().sql('select * from INFORMATION_SCHEMA.tables;');
  }

  run() {
    this.vm.run();
  }

  removeParameter(parameter) {
    this.vm.removeParameter(parameter);
  }

  addParameter() {
    this.vm.addParameter();
  }

  typelist() {
    return this.vm.typelist();
  }

  view(vnode) {
    const state = vnode.state;
    return <div class="container">
             <form class="form-inline">
               <h3>SQL</h3>
               <SQLEditor value={state.vm.query().sql()} onchange={m.withAttr('value', state.vm.query().sql)}/>
               <button onclick={state.run.bind(state)} type="button" class="btn btn-default">Run</button>
             </form>
             {state.parametersView(vnode)}
             {state.resultView(vnode)}
           </div>
  }

  parametersView(vnode) {
    const state = vnode.state;
    return <div>
      <h3>Parameters</h3>
      <table class="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Value</th>
            <th>Type</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {state.vm.query().parameters().map(p => {
             return <tr>
                     <td>
                       <input type="text" placeholder="name" value={p.name()} onchange={m.withAttr('value', p.name)} class="form-control"/>
                     </td>
                     <td>
                       <input type="text" placeholder="name" value={p.value()} onchange={m.withAttr('value', p.value)} class="form-control"/>
                     </td>
                     <td>
                       <input type="text" placeholder="name" value={p.type()} onchange={m.withAttr('value', p.type)} class="form-control" autocomplete list="type">
                         <datalist id="type">
                           {
                             state.typelist().map(type => {
                               return <option value={type}/>
                             })
                           }
                         </datalist>
                       </input>
                     </td>
                     <td>
                       <button type="button" class="btn btn-default" onclick={state.removeParameter.bind(state, p)}>Delete</button>
                     </td>
                   </tr>
           })
         }
        </tbody>
      </table>
      <button onclick={state.addParameter.bind(state)} type="button" class="btn btn-default">Add</button>
    </div>
  }

  resultView(vnode) {
    const state = vnode.state;
    return <div>
      <h3>Result</h3>
      {state.vm.error() ? <div class="alert alert-danger">{state.vm.error().message}</div> : ''}
      <div class="container">
        <p>Update Count : {state.vm.result() ? state.vm.result().updateCount() : ''}</p>
        <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
            {
              state.vm.result().columns().map(column => {
                return <th>{column.label}</th>
              })
            }
            </tr>
          </thead>
          <tbody>
            {
              state.vm.result().resultSet().map(row => {
                return <tr>
                  {
                    state.vm.result().columns().map(column => {
                      return <td>{row[column.label]}</td>
                    })
                  }
                </tr>
              })
            }
          </tbody>
        </table>
        </div>
      </div>
    </div>
  }
}

m.mount(document.getElementById('root'), App);
