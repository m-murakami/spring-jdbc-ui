'use strict';

import prop from 'mithril-stream';

export default class Result {
  constructor(data) {
    data = data || {};
    this.updateCount = prop(data.updateCount || 0);
    this.columns = prop(data.columns || []);
    this.resultSet = prop(data.resultSet || []);
  }
}