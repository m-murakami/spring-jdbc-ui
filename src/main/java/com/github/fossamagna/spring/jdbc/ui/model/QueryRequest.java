package com.github.fossamagna.spring.jdbc.ui.model;

import java.util.List;

/**
 * Represents an SQL execution request message, consisting of {@linkplain #getSql() sql}
 * and {@linkplain #getParameters() parameters}.
 * 
 * @author fossamagna
 */
public class QueryRequest {
  private String sql;
  private List<Parameter> parameters;
  
  public String getSql() {
    return sql;
  }

  public void setSql(String sql) {
    this.sql = sql;
  }

  public List<Parameter> getParameters() {
    return parameters;
  }

  public void setParameters(List<Parameter> parameters) {
    this.parameters = parameters;
  }
}