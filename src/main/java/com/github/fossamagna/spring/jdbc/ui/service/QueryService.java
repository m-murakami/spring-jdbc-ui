package com.github.fossamagna.spring.jdbc.ui.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.stereotype.Service;

import com.github.fossamagna.spring.jdbc.ui.model.Column;
import com.github.fossamagna.spring.jdbc.ui.model.Parameter;
import com.github.fossamagna.spring.jdbc.ui.model.QueryRequest;
import com.github.fossamagna.spring.jdbc.ui.model.QueryResult;


/**
 * @author fossamagna
 *
 */
@Service
public class QueryService {
  @Autowired
  private NamedParameterJdbcOperations jdbcOperations;
  
  @Autowired
  private ConversionService conversionService;
  
  public QueryResult execute(QueryRequest request) {
    return jdbcOperations.execute(prepareSql(request), convertSqlParameterSource(request), new PreparedStatementCallback<QueryResult>() {
      @Override
      public QueryResult doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
        ResultSet rs = null;
        try {
          ps.execute();
          final int updateCount = ps.getUpdateCount();
          final QueryResult result = new QueryResult();
          result.setUpdateCount(updateCount);
          if (updateCount == -1) {
            rs = ps.getResultSet();
            ResultSetMetaData metadata = rs.getMetaData();
            result.setColumns(Column.getColumns(metadata));
            result.setResultSet(new RowMapperResultSetExtractor<>(new ColumnMapRowMapper()).extractData(rs));
          }
          return result;
        } finally {
          JdbcUtils.closeResultSet(rs);
        }
      }});
  }
  
  protected String prepareSql(QueryRequest request) {
    return request.getSql();
  }
  
  protected SqlParameterSource convertSqlParameterSource(QueryRequest request) {
    if (request.getParameters() == null) {
      return EmptySqlParameterSource.INSTANCE;
    }
    final Map<String, ?> params =  request.getParameters().stream().collect(Collectors.toMap(Parameter::getName, p -> p.getConvertedValue(conversionService)));
    return new MapSqlParameterSource(params);
  }
}
