'use strict';

import prop from 'mithril-stream';

export default class Parameter {
  constructor() {
    this.name = prop('');
    this.value = prop('');
    this.type = prop(''); // Java type
  }
}
